using System.Collections;
using UnityEngine;
using System.IO.Ports;

public class ArduinoTest : MonoBehaviour
{
    private SerialPort serialPort = new SerialPort("COM3", 115200);

    private void Start()
    {
        serialPort.Open();
        StartCoroutine(CheckKeys());
    }
    
    public void Send(string txt)
    {
        serialPort.Write(txt);
    }

    public void ChangeSpeed(int newSpeed)
    {
        serialPort.Write((newSpeed+1).ToString());
    }

    private IEnumerator CheckKeys()
    {
        while (true)
        {
            //LEFT SERVO...
            if (Input.GetKey(KeyCode.A))
            {
                serialPort.Write("A");
            }
            else if(Input.GetKey(KeyCode.D))
            {
                serialPort.Write("D");
            }

            if (Input.GetKey(KeyCode.S))
            {
                serialPort.Write("S");
            }else if (Input.GetKey(KeyCode.W))
            {
                serialPort.Write("W");
            }
        
            //RIGHT SERVO...
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                serialPort.Write("J");
            }
            else if(Input.GetKey(KeyCode.RightArrow))
            {
                serialPort.Write("L");
            }

            if (Input.GetKey(KeyCode.DownArrow))
            {
                serialPort.Write("K");
            }else if (Input.GetKey(KeyCode.UpArrow))
            {
                serialPort.Write("I");
            }
        
            yield return new WaitForSeconds(0.01f);
        }
    }
    
    private void OnDestroy()
    {
        serialPort.Close();
    }
}
