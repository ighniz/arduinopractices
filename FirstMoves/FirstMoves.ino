#include <Servo.h>
#include <NewPing.h>

Servo servoLL;
Servo servoLR;
Servo servoTL;
Servo servoTR;

const int LL_INDEX = 0;
const int LR_INDEX = 1;
const int TL_INDEX = 2;
const int TR_INDEX = 3;

int timeScale = 1;
const int DELAY = 5;
const int DELAY_STEPS = 100;
const Servo ALL_SERVOS[]{servoLL, servoLR, servoTL, servoTR};

String STATE_DANCE = "dance";
String STATE_WALK = "walk";
String STATE_BACK = "back";
String STATE_IDLE = "idle";

String currentState = "";
String previousState = "";

const int IDLE_STEPS[][4]
{
    { 90,  90,  90,  90},
};
const int IDLE_STEPS_COUNT = *(&IDLE_STEPS + 1) - IDLE_STEPS;

const int DANCE_STEPS[][4]
{
    { 90,  90,  90,  90},
    { 90,  90,  45,  45},
    {135, 130,   0,  45},
    { 45, 130,   0,  45},
    {135, 130,   0,  45},
    { 45, 130,   0,  45},
    {135, 130,  45,  45},
    { 90,  90,  90,  90},
    { 90,  90, 135, 135},
    { 45, 135, 135, 180},
    { 45,  45, 135, 180},
    { 45, 135, 135, 180},
    { 45,  45, 135, 180},
    { 45, 135, 135, 135},
    { 90,  90,  90,  90},
};
const int DANCE_STEPS_COUNT = *(&DANCE_STEPS + 1) - DANCE_STEPS;

const int WALK_STEPS[][4]
{
    { 90,   90,  90,  90},
    { 70,   50,  90,  90},
    { 70,   50, 120, 120},
    { 90,   90, 120, 120},
    { 110, 130,  90,  90},
    { 110, 130,  60,  60},
    { 90,   90,  60,  60}
};
const int WALK_STEPS_COUNT = *(&WALK_STEPS + 1) - WALK_STEPS;

const int BACK_STEPS[][4]
{
    {  90,  90,  90,  90},
    {  90,  90,  50,  70},
    { 120, 120,  50,  70},
    { 120, 120,  90,  90},
    {  90,  90, 120, 120},
    {  90,  90,  90,  90},
};
const int BACK_STEPS_COUNT = *(&BACK_STEPS + 1) - BACK_STEPS;

int ll_axis = 90;
int lr_axis = 90;
int tl_axis = 90;
int tr_axis = 90;

const int servoSpeed = 1;

const int SONAR_MAXIMUM_DISTANCE = 200; //CM.
NewPing sonar(4, 3, SONAR_MAXIMUM_DISTANCE);

void setup()
{
    Serial.begin(115200);
    
    servoLL.attach(12);
    servoLR.attach(10);
    servoTL.attach(11);
    servoTR.attach(9);
}

void loop()
{
    CheckInput();
    
    if(currentState == STATE_DANCE)
    {
        ExecuteAllSteps(DANCE_STEPS, DANCE_STEPS_COUNT);
    }else if(currentState == STATE_WALK)
    {
        ExecuteAllSteps(WALK_STEPS, WALK_STEPS_COUNT);
    }else if(currentState == STATE_BACK)
    {
        ExecuteAllSteps(BACK_STEPS, BACK_STEPS_COUNT);
    }else if(currentState == STATE_IDLE)
    {
        ExecuteAllSteps(IDLE_STEPS, IDLE_STEPS_COUNT);
    }

    auto sonarCm = sonar.ping_cm();
    while(sonarCm <= 20)
    {
        ExecuteAllSteps(BACK_STEPS, BACK_STEPS_COUNT);
        sonarCm = sonar.ping_cm();
    }
}

void SetState(String newState)
{
    previousState = currentState;
    currentState = newState;
}

void CheckInput()
{
    auto serialPortData = Serial.read();
    
    // put your main code here, to run repeatedly:
    if(serialPortData == 'F')
    {
        SetState(STATE_DANCE);
    }else if(serialPortData == 'Q')
    {
        SetState(STATE_IDLE);
    }else if(serialPortData == 'E')
    {
        SetState(STATE_WALK);
    }else if(serialPortData == 'R')
    {
        SetState(STATE_BACK);
    }else if(serialPortData == 'T')
    {
        SetState("");
    }else if(serialPortData == '1')
    {
        timeScale = 1;
    }else if(serialPortData == '2')
    {
        timeScale = 2;
    }else if(serialPortData == '3')
    {
        timeScale = 3;
    }else if(serialPortData == '4')
    {
        timeScale = 4;
    }else if(serialPortData == '5')
    {
        timeScale = 5;
    }
    else
    {
        //LEFT SERVO...
        if(serialPortData == 'A')
        {
            tl_axis -= servoSpeed;
            servoTL.write(tl_axis);
        }else if(serialPortData == 'D')
        {
            tl_axis += servoSpeed;
            servoTL.write(tl_axis);
        }

        if(serialPortData == 'S')
        {
            ll_axis -= servoSpeed;
            servoLL.write(ll_axis);
        }else if(serialPortData == 'W')
        {
            ll_axis += servoSpeed;
            servoLL.write(ll_axis);
        }
        
        //RIGHT SERVO...
        if(serialPortData == 'J')
        {
            tr_axis -= servoSpeed;
            servoTR.write(tr_axis);
        }else if(serialPortData == 'L')
        {
            tr_axis += servoSpeed;
            servoTR.write(tr_axis);
        }

        if(serialPortData == 'K')
        {
            lr_axis -= servoSpeed;
            servoLR.write(lr_axis);
        }else if(serialPortData == 'I')
        {
            lr_axis += servoSpeed;
            servoLR.write(lr_axis);
        }

        ll_axis = constrain(ll_axis, 0, 180);
        lr_axis = constrain(lr_axis, 0, 180);
        tl_axis = constrain(tl_axis, 0, 180);
        tr_axis = constrain(tr_axis, 0, 180);
    }
}

const void ExecuteAllSteps(const int allSteps[][4], int stepsCount)
{
    //int stepsCount = sizeof(allSteps)/sizeof(allSteps[0]);//*(&allSteps + 1) - allSteps;
    for(int i_steps = 0; i_steps<stepsCount; i_steps++)
    {
        delay(DELAY_STEPS);
        
        float prevLL = servoLL.read();
        float prevLR = servoLR.read();
        float prevTL = servoTL.read();
        float prevTR = servoTR.read();
        
        for(int i_time = 0; i_time<=100; i_time += timeScale)
        {
            servoLL.write(map(i_time, 0, 100, prevLL, allSteps[i_steps][LL_INDEX]));
            servoLR.write(map(i_time, 0, 100, prevLR, allSteps[i_steps][LR_INDEX]));
            servoTL.write(map(i_time, 0, 100, prevTL, allSteps[i_steps][TL_INDEX]));
            servoTR.write(map(i_time, 0, 100, prevTR, allSteps[i_steps][TR_INDEX]));

            delay(DELAY);
        }
    }
}
