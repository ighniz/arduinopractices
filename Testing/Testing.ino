#include <Servo.h>
#include <NewPing.h>

#define MIN_ANGLE 45
#define MAX_ANGLE 100
#define SONAR_MAX_DISTANCE 200
#define MIN_SPEED 10
#define MAX_SPEED 100

Servo servoLL;
Servo servoLR;
Servo servoTL;
Servo servoTR;

NewPing sonar(4, 3, SONAR_MAX_DISTANCE); //Trigger Pin, Echo Pin, Max Distance (cm).

int llIdle;
int lrIdle;
int tlIdle;
int trIdle;
int i = 0;

void setup() 
{
    // put your setup code here, to run once:
    servoLL.attach(12);
    servoLR.attach(10);
    servoTL.attach(11);
    servoTR.attach(9);

    //servoLL.write(0);

    servoLL.write(90);
    servoLR.write(90);
    servoTL.write(90);
    servoTR.write(90);

    Serial.begin(115200);
}

void loop()
{
    int sonarCM = sonar.ping_cm();
    int danceFrequency = map(sonarCM, 0, SONAR_MAX_DISTANCE, 2, 50);
    
    for(i=MIN_ANGLE; i<=MAX_ANGLE; i++)
    {
        servoTL.write(i);
        servoTR.write(i);
        delay(danceFrequency);
    }

    for(i=MAX_ANGLE; i>=MIN_ANGLE; i--)
    {
        servoTL.write(i);
        servoTR.write(i);
        delay(danceFrequency);
    }
    // put your main code here, to run repeatedly:
    /*for(i=llIdle; i<=llIdle - 45; i++)
    {
        servoLL.write(i);
        delay(50);
    }*/
}

void SetIdle()
{
    
}
